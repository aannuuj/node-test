  /**
 * Module dependencies.
 */

var config = require('./config');

if (config.newrelic.enabled) {
  var newrelic = require('newrelic');
}

var express        = require('express'),
    expressHbs     = require('express-handlebars'),
    path           = require('path'),
    bodyParser     = require('body-parser'),
    compress       = require('compression'),
    favicon        = require('static-favicon'),
    methodOverride = require('method-override'),
	routes         = require('./routes'),
    Promise = require("bluebird");

var app = express(),
    server = require('http').createServer(app);

// allow CORS
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');

    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

app.engine('hbs', expressHbs({extname:'hbs', defaultLayout:'layout.hbs'}));

app.use(bodyParser.urlencoded({'extended':'true', 'limit': '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

/**
 * Express configuration.
 */
app.set('port', config.server.internalPort);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app
  .use(compress())
  .use(favicon())
  .use(bodyParser())
  .use(methodOverride())
  .use(express.static(path.join(__dirname, 'public')))
  .use(routes.router)
  .use(function (req, res) {
    res.status(404).render('404', {title: 'Not Found :('});
  });




//Prototypes and things
if (typeof String.prototype.startsWith != 'function') {
  // see below for better implementation!
  String.prototype.startsWith = function (str){
    return this.indexOf(str) === 0;
  };
}

//Setup services


var servicesPromises = [
];


  server.listen(app.get('port'), function () {
    console.log('express server listening on port ' + app.get('port'));
  });

