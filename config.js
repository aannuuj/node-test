var config = {
    server: {
      id: "environment",
      name: "demo",
      host: "192.168.1.44",
      port: "3011",
      internalPort: "3011",
      protocol : "http",
      api_key : "123123123",
      contact_email: "hello@tasito.com",
      sender_email : 'Tasito <no-reply@tasito.com>',
      web_uri: "192.168.1.44/TesitoFull/tesito_material/shop_panal/tesito_work",
      get_app_uri: "http://192.168.1.44:3011/get-the-app"
    },
    logging : {
      level : 'debug', //Can be 'debug' , 'warn', 'info', 'error'
      loggers : {
        graylog : { type :'graylog' , host : 'logs.eatfeast.com' , port : 12201 },
        local : {type: 'local' }
      }
    },
    database: {
      client:'mysql',
      connection: {
        host:'162.144.145.172',
        port:'3306',
        user: 'tasito_ulive',
        password: 'd^=O_Of[w9ar',
        database: 'tasito_live',
        charset: 'utf8'
      }
    },
    session: {
      secret: 'A Super Special And Fantastic Secret!'
    },
    AWS: {
      id: "",
      AccessKeyId: "",
      SecretKey: "",
      Region: ""
    },
    stripe: {
      id: "",
      api_key: ""
    },
  kounta:new function(){
        this.baseUrl = '';
        this.company= {
          id : 12689
        };
        this.apiBase = this.baseUrl + this.company.id +'/';
        this.auth ={
          client_id: '',
          client_secret: '',
          refresh_token: '',
          company_id:""
        };
        this.lunchCategoryId = '';
        this.dinnerCategoryId = '';
        this.categories = [
        ];
        this.priceListId = '';
        this.registerId = '';
      },
    contentful: {
      id: "",
      space: "",
      menuitemType: "",
      accessToken: "",
      oAuthToken: "",
      secure: true
    },
    google: {
      id: "",
      client_id: "",
      client_secret: "",
      api_key: "",
      APIkey: "",
      clientID: "",
      clientSecret: "",
      callbackURL: "",
      scope: ''
    },
    campaign_monitor: {
      id: "",
      apiUrl : "",
      apiKey: "",
      clientID: "",
      mailingListIds: {
        notifyListId:  "",
        promoListId:   ""
      }
    },
    sms : {
      id: "",
      apiKey: "",
      apiSecret: "",
      urlJson :"",
      fromNumber : ""
    },
    feast_api_uri: 'http://192.168.1.44:3011',
    webUri: 'http://192.168.1.44:3011',
    distance: 10,
    getAppUri:'http://192.168.1.44:3011/get-the-app',
    contactEmail: 'hello@eatfeast.com',
    web_name: "Tasito",
    replenisher: {
      kitchenId: '14138',
      previewKitchenId: '14138'
    },
    maxoptra: {
    baseUri: 'http://service2.maxoptra.com',
    lunch: {
       accountID: "",
      username: "",
      password: ""
  },
    gt: {
       accountID: "",
        username: "",
        password: ""
  }
  },
  newrelic: {
    enabled: true,
    licenseKey: "",
    appName: ""
  },
  cron :{
    pessimistic_scheduler : process.env.CRON_PESSIMISTIC_SCHEDULER == 1
  }
};

module.exports = config;

