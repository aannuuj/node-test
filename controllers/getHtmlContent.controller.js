/**
Controller for get html using bookshelf
*/
var bPromise = require('bluebird');
var FetchStream  = require("fetch").FetchStream;
var fetchUrl = require("fetch").fetchUrl;
var htmlparser = require("htmlparser");
var SummaryTool = require('node-summary');
var cheerio = require('cheerio');

exports.getComtent = function (req, res) {
	console.log("Inn");
	var urlCheck = (req.body.link)?req.body.link:false;
	var responseData = {
		"title" : "",
		"titleLength" : 0,
		"description" : "",
		"descriptionLength" : 0,
		"pageSpeed" : 0,
		"pageRatio" : 0,
		"count" : {
			"H1" : 0,
			"H2" : 0,
			"H3" : 0
		}
	}
	
	/*var fetch = new FetchStream("http://google.com");

	fetch.on("data", function(chunk){
		console.log(chunk);
	});*/
	
	
	return fetchUrl(urlCheck, function(error, meta, body){
		console.log(meta);
		var rawHtml = body.toString();
		
		// calculating h1 count
		responseData.count.H1 = rawHtml.split("<h1").length-1;
		responseData.count.H2 = rawHtml.split("<h2").length-1;
		responseData.count.H3 = rawHtml.split("<h3").length-1;
		console.log(rawHtml.split("<h3").length);
		var handler = new htmlparser.DefaultHandler(function (error, dom) {
			if (error)
				console.log(error);
			else
				console.log("data");
		});
		
		var parser = new htmlparser.Parser(handler);
		parser.parseComplete(rawHtml);
		
		var domRoot = handler.dom;
		//console.log(domRoot);
		
		var childrenList = domRoot[3].children;
		
		//console.log(childrenList);
		//res.json({ "data" : childrenList})
		return bPromise.all(childrenList).each(function(item){
			//console.log(item.raw);
			if(item.raw == "head"){
				return bPromise.map(item.children, function(item1){
					//console.log(item1);
					if(item1.raw == "title"){
						var title = item1.children[0].data;
						responseData.title = title;
						responseData.titleLength = title.length;
					}
					if(item1.name == "meta"){
						if(item1.attribs.name == "description"){
							var description = item1.attribs.content;
							responseData.description = description;
							responseData.descriptionLength = description.length;
						}	
					}
				});
			}
		}).then(function(data){
			/*psi(urlCheck).then(function(data1){
				responseData.pageSpeed = data.pageStatus;
			});*/
			
			
			return SummaryTool.summarize(responseData.title, rawHtml, function(err, summary) {
				if(err) console.log("Something went wrong man!");

				console.log("Original Length " + (responseData.title.length + rawHtml.length));
				console.log("Summary Length " + summary.length);
				console.log("Summary Ratio: " + (100 - (100 * (summary.length / (responseData.title.length + rawHtml.length)))));
				
				responseData.pageRatio = (100 - (100 * (summary.length / (responseData.title.length + rawHtml.length))));
				
				//$ = cheerio.load(rawHtml);
				//res.json({ "data" : $('img')})
				//responseData.pageRatio = responseData.pageRatio+"%"
				res.json({ "data" : responseData})
			});
		});
		//res.json({ "data" : childrenList})
	});
}