/**
 * Module dependencies
 */
var controller = require('../controllers/getHtmlContent.controller');

/**
 * the new Router exposed in express 4
 * the indexRouter handles all requests to the `/` path
 */
module.exports = function(router) {
  /**
   * this accepts all request methods to the `/` path
   */
  router.route('/getHtmlContent/')
    .post(controller.getComtent);
}
